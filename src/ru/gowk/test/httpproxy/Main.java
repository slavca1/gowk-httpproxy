package ru.gowk.test.httpproxy;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;

/**
 * Created on Dec 09, 2016
 *
 * @author Vyacheslav Gorbatykh
 */
public class Main {
    public static void main(String[] args) {
        BrowserMobProxy proxy = new BrowserMobProxyServer();
        proxy.start(8888);

        proxy.addRequestFilter(new RequestFilter() {
            @Override
            public io.netty.handler.codec.http.HttpResponse filterRequest(io.netty.handler.codec.http.HttpRequest httpRequest, HttpMessageContents httpMessageContents, HttpMessageInfo httpMessageInfo) {
                System.out.println("httpRequest = " + httpRequest);
//                System.out.println("httpMessageContents = " + httpMessageContents);
//                System.out.println("httpMessageInfo = " + httpMessageInfo);
                return null;
            }
        });
    }
}
